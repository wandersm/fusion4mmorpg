package worlddb

type SObjPrototype_Flags struct {
	IsExclusiveTrigger       bool `json:"isExclusiveTrigger,omitempty"`
	IsRemoveAfterTriggerDone bool `json:"isRemoveAfterTriggerDone,omitempty"`
	IsPlayerInteresting      bool `json:"isPlayerInteresting,omitempty"`
	IsCreatureInteresting    bool `json:"isCreatureInteresting,omitempty"`
	IsActivity               bool `json:"isActivity,omitempty"`
}

type SObjPrototype_CostItem struct {
	ItemId  uint32 `json:"itemId,omitempty"`
	ItemNum uint32 `json:"itemNum,omitempty"`
}

type SObjPrototype struct {
	SobjTypeId uint32              `json:"sobjTypeId,omitempty" rule:"required"`
	SobjFlags  SObjPrototype_Flags `json:"sobjFlags,omitempty" rule:"required"`

	MinRespawnTime uint32 `json:"minRespawnTime,omitempty" rule:"required"`
	MaxRespawnTime uint32 `json:"maxRespawnTime,omitempty" rule:"required"`

	TeleportPointID   uint32 `json:"teleportPointID,omitempty" rule:"required"`
	TeleportDelayTime uint32 `json:"teleportDelayTime,omitempty" rule:"required"`

	LootSetID        uint32                   `json:"lootSetID,omitempty" rule:"required"`
	TriggerSpellID   uint32                   `json:"triggerSpellID,omitempty" rule:"required"`
	TriggerSpellLv   uint32                   `json:"triggerSpellLv,omitempty" rule:"required"`
	TriggerAnimTime  uint32                   `json:"triggerAnimTime,omitempty" rule:"required"`
	TriggerCostItems []SObjPrototype_CostItem `json:"triggerCostItems,omitempty" rule:"required"`

	SobjReqQuestDoing uint32 `json:"sobjReqQuestDoing,omitempty" rule:"required"`

	Radius float32 `json:"radius,omitempty" rule:"required"`

	SpawnScriptId   uint32 `json:"spawnScriptId,omitempty" rule:"required"`
	SpawnScriptArgs string `json:"spawnScriptArgs,omitempty" rule:"required"`
	PlayScriptId    uint32 `json:"playScriptId,omitempty" rule:"required"`
	PlayScriptArgs  string `json:"playScriptArgs,omitempty" rule:"required"`
}

func (*SObjPrototype) GetTableName() string {
	return "sobj_prototype"
}
func (*SObjPrototype) GetTableKeyName() string {
	return "sobjTypeId"
}
func (obj *SObjPrototype) GetTableKeyValue() uint {
	return uint(obj.SobjTypeId)
}

type StaticObjectSpawn_Flags struct {
	IsRespawn     bool `json:"isRespawn,omitempty"`
	IsPlaceholder bool `json:"isPlaceholder,omitempty"`
}

type StaticObjectSpawn struct {
	SpawnId  uint32                  `json:"spawnId,omitempty" rule:"required"`
	Flags    StaticObjectSpawn_Flags `json:"flags,omitempty" rule:"required"`
	Entry    uint32                  `json:"entry,omitempty" rule:"required"`
	Map_id   uint32                  `json:"map_id,omitempty" rule:"required"`
	Map_type uint32                  `json:"map_type,omitempty" rule:"required"`
	X        float32                 `json:"x,omitempty" rule:"required"`
	Y        float32                 `json:"y,omitempty" rule:"required"`
	Z        float32                 `json:"z,omitempty" rule:"required"`
	O        float32                 `json:"o,omitempty" rule:"required"`
	Radius   float32                 `json:"radius,omitempty" rule:"required"`
}

func (*StaticObjectSpawn) GetTableName() string {
	return "staticobject_spawn"
}
func (*StaticObjectSpawn) GetTableKeyName() string {
	return "spawnId"
}
func (obj *StaticObjectSpawn) GetTableKeyValue() uint {
	return uint(obj.SpawnId)
}

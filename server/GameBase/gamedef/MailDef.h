#pragma once

#define MAIL_EXPIRE_TIME (60*60*24*30)

#define MailFlag_Args ((u32)MailFlag::SubjectArgs|(u32)MailFlag::BodyArgs)

inst_mail NewSystemMailInstance(uint32 receiver,
	uint32 flags, const std::string& subject, const std::string& body);
void AppendCheque2MailInstance(
	inst_mail& mailProp, uint8 chequeType, uint64 chequeValue);
void AppendCheque2MailInstance(
	inst_mail& mailProp, const std::string& chequeProp);
void AppendItem2MailInstance(
	inst_mail& mailProp, uint32 itemTypeID, uint32 itemCount);
void AppendItem2MailInstance(
	inst_mail& mailProp, const std::string& itemProp);

#pragma once

#define ITEM_CURRENCY_TYPE (100)
#define ITEM_CHEQUE_TYPE (100)

#define ITEM_BIND_TO_PLAYER (1)
#define ItemSlotInvalid (u32(-1))

enum ItemSlotType
{
	ItemSlotEquipType,
	ItemSlotBagType,
	ItemSlotBankType,
	ItemSlotTypeCount
};

enum ItemEquipSlot
{
	ItemEquipSlotCount = 12,
};

#define ItemBagSlotDefCount (30u)
#define ItemBagSlotCount (256u)
#define ItemBankSlotDefCount (30u)
#define ItemBankSlotCount (256u)

enum ItemCountFlag
{
	ICF_PREVENT_SYNC_CLIENT = 1 << 0,
	ICF_PREVENT_SYNC_VALUE = 1 << 1,
};

enum ItemStorageFlag
{
	ISF_INCLUDE_EQUIP = 1 << 0,
	ISF_INCLUDE_BAG = 1 << 1,
	ISF_INCLUDE_BANK = 1 << 2,
	ISF_INCLUDE_BINDING = 1 << 3,
	ISF_INCLUDE_NONBINDING = 1 << 4,
	ISF_EXCLUDE_EQUIP = 1 << 8,
	ISF_EXCLUDE_BAG = 1 << 9,
	ISF_EXCLUDE_BANK = 1 << 10,
	ISF_EXCLUDE_BINDING = 1 << 11,
	ISF_EXCLUDE_NONBINDING = 1 << 12,
};

enum ITEM_FLOW_TYPE
{
	IFT_NONE,
	IFT_SCRIPT,  // 脚本
	IFT_PAY,  // 充值
	IFT_MAIL,  // 邮件
	IFT_MINE,  // 采集
	IFT_SHOP,  // 商店
	IFT_LOOT,  // 怪物掉落
	IFT_ITEM_USE,  // 正常使用
	IFT_ITEM_DESTROY,  // 丢弃物品
	IFT_ITEM_ARRANGE,  // 整理物品
	IFT_ITEM_SPLIT,  // 拆分物品
	IFT_ITEM_SWAP,  // 交换物品
	IFT_ITEM_EQUIP,  // 装备物品
	IFT_ITEM_UNEQUIP,  // 卸下物品
	IFT_QUEST_ACCEPT,  // 接受任务
	IFT_QUEST_CANCEL,  // 取消任务
	IFT_QUEST_SUBMIT,  // 提交任务
	IFT_OPERATING_BEGIN,  // 运营活动
	IFT_OPERATING_END = IFT_OPERATING_BEGIN + 100,
};

struct inst_item_prop
{
	inst_item_prop(uint32 itemTypeID = 0, uint32 itemCount = 0);

	enum class Status {
		Constant,
	};

	uint32 itemGuid;
	uint32 itemTypeID;
	uint32 itemCount;
	uint32 itemOwner;
	uint32 itemStatus;

	void Save(INetStream& pck) const;
	void Load(INetStream& pck);

	void Save(TextPacker& packer) const;
	void Load(TextUnpacker& unpacker);

	std::string Save() const;
	void Load(const char* data);
};

inst_item_prop NewItemProp4Owner(
	uint32 itemTypeID, uint32 itemCount, uint32 itemOwner);
inst_item_prop NewItemProp4Flags(
	uint32 itemTypeID, uint32 itemCount, uint32 itemFlags);

std::vector<inst_item_prop> NewItemProps4ItemInfo(
	const std::vector<ItemInfo>& itemInfos);
std::vector<inst_item_prop> NewItemProps4FItemInfo(
	const std::vector<FItemInfo>& itemInfos);

bool IsItemProtoEquipSlotValid(const ItemPrototype* pItemProto, uint32 slot);
std::pair<uint32, uint32> ItemProto2EquipSlot(const ItemPrototype* pItemProto);

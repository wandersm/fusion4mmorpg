#include "jsontable/table_helper.h"
#include "struct_attribute.h"

PlayerBase::PlayerBase()
: level(0)
, lvUpExp(0)
, recoveryHPRate(.0)
, recoveryHPValue(.0)
, recoveryMPRate(.0)
, recoveryMPValue(.0)
{
}

PlayerAttribute::PlayerAttribute()
: Id(0)
, career(0)
, level(0)
, damageFactor(.0)
{
}

CreatureAttribute::CreatureAttribute()
: Id(0)
, type(0)
, level(0)
, round(0)
, damageFactor(.0)
{
}

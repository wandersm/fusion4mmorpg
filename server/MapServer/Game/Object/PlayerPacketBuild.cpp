#include "preHeader.h"
#include "Player.h"

void Player::BuildCreatePacketForPlayer(INetPacket& pck, Player* pPlayer)
{
	Unit::BuildCreatePacketForPlayer(pck, pPlayer);
	pck << m_ipcInfo.ipcNickName;
	if (this == pPlayer) {
		m_pItemStorage->BuildCreatePacketForPlayer(pck, pPlayer);
		m_pQuestStorage->BuildCreatePacketForPlayer(pck, pPlayer);
		m_pVipStorage->BuildCreatePacketForPlayer(pck, pPlayer);
	}
}

std::string Player::PackOperatingPlayerInfo() const
{
	NetBuffer buffer;
	buffer << GetGuidLow()
		<< m_pVipStorage->GetLevel() << GetLevel();
	return buffer.CastBufferString();
}

std::string Player::PackClientAddr4Cross() const
{
	NetBuffer buffer;
	ClientAddr4Cross clientAddr;
	clientAddr.guid = m_pGateServerSession->guid();
	clientAddr.sn = m_clientSN;
	buffer << clientAddr;
	return buffer.CastBufferString();
}

#include "preHeader.h"
#include "AttrPartProxy.h"
#include "Unit.h"

AttrPartProxy::AttrPartProxy(Unit* pOwner, ATTRPARTTYPE part)
: m_pOwner(pOwner)
, m_part(part)
{
}

AttrPartProxy::~AttrPartProxy()
{
}

void AttrPartProxy::Reset()
{
	for (auto& info : m_availSpellInfos) {
		m_pOwner->PopPassiveSpell(info.id, info.level);
	}
	m_availSpellInfos.clear();
}

void AttrPartProxy::ApplyAttribute(uint32 type, double value)
{
	m_pOwner->GetAttribute().ModXAttr(m_part, type, value);
}

void AttrPartProxy::ApplySpell(uint32 id, uint32 level)
{
	m_availSpellInfos.push_back({id, level});
	m_pOwner->PushPassiveSpell(id, level);
}

#pragma once

#include "Singleton.h"
#include "network/Listener.h"

class ClientListener : public Listener, public Singleton<ClientListener>
{
public:
	THREAD_RUNTIME(ClientListener)

	ClientListener();
	virtual ~ClientListener();

private:
	virtual bool BindSocketReady(SOCKET sockfd);

	virtual std::string GetBindAddress();
	virtual std::string GetBindPort();

	virtual Session *NewSessionObject();

	uint32 m_sn;
};

#define sClientListener (*ClientListener::instance())

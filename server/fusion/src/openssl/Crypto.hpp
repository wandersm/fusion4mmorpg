#pragma once

#include <openssl/rsa.h>
#include <string>
#include <string_view>

namespace rsa {

RSA *generate_key();
void free_key(RSA *rsa);

std::string dump_pubkey(RSA *rsa);

std::string public_encrypt(
    const std::string_view &pubkey, const std::string_view &data);
std::string private_decrypt(RSA *rsa, const std::string_view &data);

}

namespace hex {

std::string dump(const std::string_view &data);

}

namespace sha {

std::string feed256(const std::string_view &data);
std::string feed512(const std::string_view &data);

}

namespace md5 {

std::string feed(const std::string_view &data);

}

namespace base64 {

std::string encode(const std::string_view &data);
std::string decode(const std::string_view &data);

}

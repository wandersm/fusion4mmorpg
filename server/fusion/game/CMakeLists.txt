set(CMAKE_CXX_FLAGS "-Wall -pthread")
set(CMAKE_CXX_STANDARD 11)

include_directories(. ../src ../external ../feature ../thirdparty/lua/include)

aux_source_directory(ai AI_LIST)
aux_source_directory(aoi AOI_LIST)
aux_source_directory(tile TILE_LIST)
add_library(game STATIC ${AI_LIST} ${AOI_LIST} ${TILE_LIST})

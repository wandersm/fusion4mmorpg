#include "preHeader.h"
#include "GameServer.h"
#include "Game/CharacterMgr.h"
#include "Game/TeleportMgr.h"
#include "Manager/InstCfgMgr.h"
#include "Manager/DataMgr.h"
#include "Manager/SpecialShopMgr.h"
#include "Manager/GuildLeagueMgr.h"
#include "Mail/MailMgr.h"
#include "WordFilter/WordFilter.h"
#include "Session/MapServerMgr.h"
#include "ServerMaster.h"

GameServer::GameServer()
: sWheelTimerMgr(1, GET_UNIX_TIME)
, m_isServing(false)
{
}

GameServer::~GameServer()
{
}

WheelTimerMgr *GameServer::GetWheelTimerMgr()
{
	return &sWheelTimerMgr;
}

bool GameServer::LoadData()
{
	sWordFilter.Init();
	sMailMgr.Init();
	sGuildLeagueMgr.Init();
	sDataMgr.LoadData();
	sSpecialShopMgr.LoadData();
	sGuildLeagueMgr.LoadData();
	sMapServerMgr.LoadMapServerConfig();
	return true;
}

bool GameServer::Initialize()
{
	if (!sInstCfgMgr.LoadDataFromDB()) {
		ELOG("Server Load InstCfg Data Error!!!");
		IServerMaster::GetInstance().End(0);
		return false;
	}
	if (!sCharacterMgr.LoadDataFromDB()) {
		ELOG("Server Load Character Data Error!!!");
		IServerMaster::GetInstance().End(0);
		return false;
	}
	if (!LoadData()) {
		ELOG("Server Load Data Error!!!");
		IServerMaster::GetInstance().End(0);
		return false;
	}
	m_isServing = true;
	NLOG("Game Server Start Serving ...");
	return FrameWorker::Initialize();
}

void GameServer::Finish()
{
	sSessionManager.Stop();
	return FrameWorker::Finish();
}

void GameServer::Update(uint64 diffTime)
{
	AsyncTaskOwner::UpdateTask();
	sSessionManager.Update();
}

void GameServer::OnTick()
{
	sSessionManager.Tick();
	sTeleportMgr.Tick();
	sWheelTimerMgr.Update(GET_UNIX_TIME);
}
